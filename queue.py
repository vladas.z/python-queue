#!env/bin/python

class queue:
    _inner_list = []

    def __init__(self, **args):
        self._inner_list = args.values()

    def pop(self):
        if len(self._inner_list) == 0:
            return None

        element = self._inner_list[0]
        del self._inner_list[0]
        
        return element